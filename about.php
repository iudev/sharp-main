<?php
//index.php 
require_once 'includes/global.inc.php';
$page = "about.php"
?>
<html>
	<head>
		<title>О проекте | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		
		<main role="main">
		<?php $user = unserialize($_SESSION['user']); ?>

		<?php if(isset($_SESSION['logged_in'])) : ?>
			
		<?php else : ?>
			
		<?php endif; ?> 
		<center>
		<h1>Проект "ШАРП"</h1>

		<h4>Республиканский проект по привлечению и развития интереса детей к программированию и ИТ-сфере.</h4>

		Находимся в режиме развития в Удмуртской республике.<br><br>

		Организации - авторы проекта:<br>
		МБОУ ИТ-лицей №24 г. Ижевск<br>
		АУ УР "РЦИ и ОКО", Удмуртская республика (Детский технопарк "Кванториум")<br><br>

		Проводим соревнования по программированию, системному администрированию, веб-разработке,<br>а также создаем онлайн-курсы по всем направлениям.
		<br><br>
		<h3>Команда проекта</h3>
		<div class="container">
          <div class="row">
            <div class="col-sm">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="" alt="Card image cap">
                  <div class="card-body">
                      <h5 class="card-title">Даниил Олегович Бондарь</h5>
                    <p class="card-text">Автор проекта</p>
                  </div>
                </div>
            </div>
            <div class="col-sm">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="" alt="Card image cap">
                  <div class="card-body">
                      <h5 class="card-title">Данил Дмитриевич Ломаев</h5>
                    <p class="card-text">Соавтор проекта</p>
                  </div>
                </div>
            </div>
            <div class="col-sm">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="" alt="Card image cap">
                  <div class="card-body">
                      <h5 class="card-title">Оксана Васильевна Щелчкова</h5>
                    <p class="card-text">Куратор проекта</p>
                  </div>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="" alt="Card image cap">
                  <div class="card-body">
                      <h5 class="card-title">Ольга Михайловна Грудцина</h5>
                    <p class="card-text">Куратор проекта <small>со стороны МБОУ "ИТ-лицей №24"</small></p>
                  </div>
                </div>
            </div>
            <div class="col-sm">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="" alt="Card image cap">
                  <div class="card-body">
                      <h5 class="card-title">Дмитрий Александрович Дунаев</h5>
                    <p class="card-text">Руководитель направления "Программирование"</p>
                  </div>
                </div>
            </div>
            <div class="col-sm">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="" alt="Card image cap">
                  <div class="card-body">
                      <h5 class="card-title">Егор Станиславович Воронцов</h5>
                    <p class="card-text">Эксперт направления "Программирование"</p>
                  </div>
                </div>
            </div>
          </div>
        </div>
		</main>
		</center>
		</body>
</html>