<?php
//index.php 
require_once 'includes/global.inc.php';
$page = "competitions.php"
?>
<html>
	<head>
		<title>Соревнования | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		
		<main role="main">
		<?php $user = unserialize($_SESSION['user']); ?>
		
		<?php if(isset($_SESSION['logged_in'])) : ?>
			<h1>Соревнования</h1>
			На данной странице публикуются активные для регистрации соревнования.<br>
			<br><strong>Фазы соревнований и их цвета:</strong><br>
			<div class="progress">
			  <div class="progress-bar bg-dark" role="progressbar" style="width: 16%" aria-valuenow="16" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="">Планируется</div>
			  <div class="progress-bar bg-info" role="progressbar" style="width: 16%" aria-valuenow="16" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="Идет подготовка">Подготовка</div >
			  <div class="progress-bar bg-success" role="progressbar" style="width: 16%" aria-valuenow="16" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="Идет регистрация">Регистрация</div>
			  <div class="progress-bar bg-warning" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="Регистрация завершена">Регистрация закрыта</div>
			  <div class="progress-bar bg-danger" role="progressbar" style="width: 16%" aria-valuenow="16" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="Обновлена важная информация">Внимание!</div>
			  <div class="progress-bar bg-success" role="progressbar" style="width: 16%" aria-valuenow="16" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="Соревнование завершено">Завершено</div>
			</div>
			<br><br>
			<div class="alert alert-info" role="alert">
				  <strong>Отладочный Турнир 2018-2019</strong>&nbsp;&nbsp;<span class="badge badge-info">Рейтинговый</span>&nbsp;<span class="badge badge-primary">Div. 1 — Div. 4</span>&nbsp;<span class="badge badge-success">Разработка задач</span><br>
				  <p>Турнир необходим для отладки внутренних бизнес-процессов</p>
				  <hr>
				  <small>
 				 <p class="mb-0">Регистрация откроется в ближайшее время</p></small>
				</div>	
		<?php else : ?>
		<div class="alert alert-danger" role="alert">
				  <strong>Ошибка безопасности #003</strong><br>
				  <p>Просмотр соревнований доступен тольго зарегистрированным участникам.</p>
				  <hr>
				  <small>
 				 <p class="mb-0">There was competitions.php GET request without active session<br>
				 Был совершен GET запрос на страницу соревнований (competitions.php) без активной сессии (пользователь не вошел)</p></small>
				</div>	
		<?php endif; ?>
		</main>
		</body>
</html>