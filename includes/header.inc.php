<header>
	<?php $user = unserialize($_SESSION['user']); ?>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="">ШАРП</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item <?php echo ($page == "index.php" ? "active" : "");?>">
              <a class="nav-link" href="index.php">Главная</a>
            </li>
            <li class="nav-item <?php echo ($page == "about.php" ? "active" : "");?>">
              <a class="nav-link" href="about.php">О проекте</a>
            </li>
            <li class="nav-item <?php echo ($page == "introduction.php" ? "active" : "");?>">
              <a class="nav-link" href="introduction.php">Вступление</a>
            </li>
			<li class="nav-item <?php echo ($page == "study.php" ? "active" : "");?>">
              <a class="nav-link" href="study.php">Обучение</a>
            </li>
			<li class="nav-item <?php echo ($page == "competitions.php" ? "active" : "");?>">
              <a class="nav-link" href="competitions.php">Соревнования</a>
            </li>
			<?php if($user->admin != 0) : ?>
			<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Админ-панель</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
				<h6 class="dropdown-header">Основное</h6>
              <a class="dropdown-item" href="admin.php">Основная панель</a>
			  <div class="dropdown-divider"></div>
			  <h6 class="dropdown-header">Статистика пользователей</h6>
              <a class="dropdown-item" href="add_points.php">Добавление баллов</a>
              <a class="dropdown-item" href="rem_points.php">Снятие баллов</a>
			  <a class="dropdown-item" href="div_change.php">Смена дивизиона</a>
			  <a class="dropdown-item" href="custom.php">Ручные уведомления</a>
            </div>
          </li>
			<!--- <li class="nav-item <?php echo ($page == "admin.php" ? "active" : "");?>">
            //  <a class="nav-link" href="admin.php">Админ-панель</a>
            //</li> -->
			<?php endif; ?>
			<?php if($user->admin != 0 || $user->acc_type == "Учитель") : ?>
			<li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Панель учителя</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
				<h6 class="dropdown-header">Основное</h6>
              <a class="dropdown-item" href="mypupils.php">Мои ученики</a>
            </div>
          </li>
			<!--- <li class="nav-item <?php echo ($page == "admin.php" ? "active" : "");?>">
            //  <a class="nav-link" href="admin.php">Админ-панель</a>
            //</li> -->
			<?php endif; ?>
          </ul>
		<?php if(isset($_SESSION['logged_in'])) : ?>
			<a class="btn btn-outline-info my-2 my-sm-0" type="button" href="settings.php">Аккаунт</a>
		  	<a class="btn btn-outline-danger my-2 my-sm-0" type="button" href="logout.php">Выход</a>
		<?php else : ?>
			 <a class="btn btn-outline-success my-2 my-sm-0" type="button" href="login.php">Вход</a>
		  	<a class="btn btn-outline-info my-2 my-sm-0" type="button" href="register.php">Регистрация</a>
		<?php endif; ?>
          </form>
        </div>
      </nav>
    </header>