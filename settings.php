<?php 

require_once 'includes/global.inc.php';
$page = "settings.php";

//Проверить вошел ли пользователь
if(!isset($_SESSION['logged_in'])) {
header("Location: login.php");
}


//взять объект user из сессии
$user = unserialize($_SESSION['user']);


//инициализировать php переменные, используемые в форме
$email = $user->email;
$username = $user->username;
$familiya = $user->familiya;
$imya = $user->imya;
$otchestvo = $user->otchestvo;
$data_rojd = $user->data_rojd;
$join_date = $user->join_date;
$school = $user->school;
$classs = $user->classs;
$coach = $user->coach;
$acc_type = $user->acc_type;
$division = $user->division;
$points = $user->points;
$error = "";
$message = "";


//проверить отправлена ли форма
if(isset($_POST['submit-settings'])) { 
//достать переменные $_POST 
$email = $_POST['email'];


$user->email = $email;
$user->save();


$message = "Settings Saved<br/>";
}
//Если форма не отправлена или не прошла проверку - показать форму снова

?>
<html>
	<head>
		<title>Аккаунт | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		<main role="main">
			<center>
			<br><br>
			<h2><?php echo $user->familiya; ?>&nbsp;<?php echo $user->imya; ?>&nbsp;<?php echo $user->otchestvo; ?></h2>
			<h3><?php echo $user->username; ?></h3>
			<h4>E-mail: <?php echo $user->email; ?></h4><br>
			<h5>Сведения об участнике:</h5>
			<strong>Тип участия: </strong> <?php echo $user->acc_type; ?><br>
			<strong>Дата рождения: </strong> <?php echo $user->data_rojd; ?><br>
			<strong>Дата и время регистрации: </strong> <?php echo $user->joinDate; ?><br>
			<strong>Школа: </strong> <?php echo $user->data_rojd; ?><br>
			<strong>Класс: </strong> <?php echo $user->classs; ?><br>
			<strong>Тренер: </strong> <?php echo $user->coach; ?><br><br>
			<h5>Рейтинговые сведения:</h5>
			<strong>Дивизион: </strong> <?php echo $user->division; ?><br>
			<strong>Баллы: </strong> <?php echo $user->points; ?><br>
			<br>
			<h5>История изменения данных (последние 10 действий):</h5>
			<?php
			 if ($result = $db->select_res('log','username = "'.$user->username.'"')) {
            echo '<table class="table">' .
            '<thead>' .
            '<tr>' .
            '<th>Дата</th>' .
            '<th>Администратор</th>' .
            '<th>Тип</th>' .
            '<th>Содержание</th>' .
            '</tr>' .
            '</thead>';
        while( $row = mysql_fetch_assoc($result)){
            echo '<tr>' .
                '<td>' . $row['date'] . '</td>' .
                '<td>' . $row['admin'] . '</td>' .
                '<td>' . $row['type'] . '</td>' .
                '<td>' . $row['text'] . '</td>' .
                '</tr>';
        }
			 }
        echo '</table>';
			?>
			</center>
		</main>
	</body>
</html>