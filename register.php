<?php
//register.php

require_once 'includes/global.inc.php';
$page = "register.php";

//инициализируем php переменные, которые используются в форме
$username = "";
$password = "";
$password_confirm = "";
$email = "";
$familiya = "";
$imya = "";
$otchestvo = "";
$data_rojd = "";
$school = "";
$classs = "";
$coach = "";
$acc_type = "";
$error = "";

//проверить отправлена ли форма
if(isset($_POST['submit-form'])) { 

//получить переменные $_POST
$username = $_POST['username'];
$password = $_POST['password'];
$password_confirm = $_POST['password-confirm'];
$email = $_POST['email'];
$familiya = $_POST['familiya'];
$imya = $_POST['imya'];
$otchestvo = $_POST['otchestvo'];
$data_rojd = $_POST['data_rojd'];
$school = $_POST['school'];
$classs = $_POST['classs'];
$coach = $_POST['coach'];
$acc_type = $_POST['acc_type'];

//инициализировать переменные для проверки формы
$success = true;
$userTools = new UserTools();

//проверить правильность заполнения формы
//проверить не занят ли этот логин
if($userTools->checkUsernameExists($username))
{
$error .= "Данное имя пользователя уже занято <br>";
$success = false;
}

//проверить совпадение паролей
if($password != $password_confirm) {
$error .= "Пароли не совпадают";
$success = false;
}

if($success)
{
//подготовить информацию для сохранения объекта нового пользователя
$data['username'] = $username;
$data['password'] = md5($password); //зашифровать пароль для хранения
$data['email'] = $email;
$data['familiya'] = $familiya;
$data['imya'] = $imya;
$data['otchestvo'] = $otchestvo;
$data['data_rojd'] = $data_rojd;
$data['school'] = $school;
$data['classs'] = $classs;
$data['coach'] = $coach;
$data['acc_type'] = $acc_type;

//создать новый объект пользователя
$newUser = new User($data);

//сохранить нового пользователя в БД
$newUser->save(true);

//войти
$userTools->login($username, $password);

mail($email, "Регистрация на сайте ШАРП", "Здравствуйте!\nТолько что Вы прошли регистрацию на сайте sharp.iu-dev.com\nСсылка для прохождения второго этапа регистрации будет отправлена позднее.");

//редирект на страницу приветствия
header("Location: welcome.php");
}
}

//Если форма не отправлена или не прошла проверку, тогда показать форму снова

?>
<html>
<head>
<title>Регистрация | ШАРП</title>
<?php require_once 'includes/bootstrap.inc.php'; ?>
</head>
<body>
<?php require_once 'includes/header.inc.php'; ?>

<main role="main">
<?php if(isset($_SESSION['logged_in'])) : ?>
			<div class="alert alert-danger" role="alert">
				  <strong>Ошибка безопасности #001</strong><br>
				  <p>Вы уже вошли в систему.</p>
				  <hr>
				  <small>
 				 <p class="mb-0">There was login.php GET request when $_SESSION is active.<br>
				 Был совершен GET запрос на страницу входа (login.php) с активной сессией $_SESSION.</p></small>
				</div>
		<?php else : ?>
			<center>
			<h3>I. Сведения об аккаунте</h3>
			<form class="form-horizontal" action="register.php" method="post">
			<fieldset>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="login">Логин</label>  
			  <div class="col-md-4">
			  <input id="login" name="username" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $username; ?>"/> 
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="password">Пароль</label>
			  <div class="col-md-4">
			    <input id="password" name="password" type="password" placeholder="" class="form-control input-md" required="" value="<?php echo $password; ?>"/>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="password">Пароль (повторите)</label>
			  <div class="col-md-4">
			    <input id="password-confirm" name="password-confirm" type="password" placeholder="" class="form-control input-md" required="" value="<?php echo $password_confirm; ?>"/>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="login">E-mail</label>  
			  <div class="col-md-4">
			  <input id="email" name="email" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $email; ?>"/> 
			  </div>
			</div>
			<br/>
			<br>
			<h3>II. Сведения об участнике</h3>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="familiya">Фамилия</label>  
			  <div class="col-md-4">
			  <input id="familiya" name="familiya" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $familiya; ?>"/>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="imya">Имя</label>  
			  <div class="col-md-4">
			  <input id="imya" name="imya" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $imya; ?>"/>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="otchestvo">Отчество</label>  
			  <div class="col-md-4">
			  <input id="otchestvo" name="otchestvo" type="text" placeholder="" class="form-control input-md" value="<?php echo $otchestvo; ?>"/>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="data_rojd">Дата рождения</label>  
			  <div class="col-md-4">
			  <input id="data_rojd" name="data_rojd" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $data_rojd; ?>"/>
			  <span class="help-block">В формате 2018-07-31</span>  
			  </div>
			</div>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="acc_type">Тип регистрации</label>
			  <div class="col-md-4">
			  <div class="radio">
				<label for="acc_type-0">
				  <input type="radio" name="acc_type" id="acc_type-0" value="<?php echo $acc_type = 'Ученик' ?>" checked="checked">
				  Ученик
				</label>
				</div>
			  <div class="radio">
				<label for="acc_type-1">
				  <input type="radio" name="acc_type" id="acc_type-1" value="<?php echo $acc_type = 'Учитель' ?>">
				  Учитель
				</label>
				</div>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="school">Школа</label>  
			  <div class="col-md-4">
			  <input id="school" name="school" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $school; ?>"/>
			  <span class="help-block">Полное юридическое название</span>  
			  </div>
			</div>

			<!-- Multiple Radios (inline) -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="classs">Класс</label>
			  <div class="col-md-4"> 
				<label class="radio-inline" for="classs-0">
				  <input type="radio" name="classs" id="classs-0" value="<?php echo $classs = '5' ?>" checked="checked">
				  5
				</label> 
				<label class="radio-inline" for="classs-1">
				  <input type="radio" name="classs" id="classs-1" value="<?php echo $classs = '6' ?>">
				  6
				</label> 
				<label class="radio-inline" for="classs-2">
				  <input type="radio" name="classs" id="classs-2" value="<?php echo $classs = '7' ?>">
				  7
				</label> 
				<label class="radio-inline" for="classs-3">
				  <input type="radio" name="classs" id="classs-3" value="<?php echo $classs = '8' ?>">
				  8
				</label> 
				<label class="radio-inline" for="classs-4">
				  <input type="radio" name="classs" id="classs-4" value="<?php echo $classs = '9' ?>">
				  9
				</label> 
				<label class="radio-inline" for="classs-5">
				  <input type="radio" name="classs" id="classs-5" value="<?php echo $classs = '10' ?>">
				  10
				</label> 
				<label class="radio-inline" for="classs-6">
				  <input type="radio" name="classs" id="classs-6" value="<?php echo $classs = '11' ?>">
				  11
				</label>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="coach">Тренер</label>  
			  <div class="col-md-4">
			  <input id="coach" name="coach" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $coach; ?>"/>
			  <span class="help-block">ФИО полностью</span>  
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-4 control-label" for="submit"></label>
			  <div class="col-md-4">
			    <button value="Register" id="submit" name="submit-form" class="btn btn-success">Регистрация</button>
			  </div>
			</div>
			</fieldset>
			</form>
			<?php if($error != "") : ?>
			<div class="alert alert-danger" role="alert">
			  <strong>Ошибка</strong><br>
			  <?php echo $error; ?>
			<?php endif; ?>
		</center>
		<?php endif; ?>
		</main>
</body>
</html>