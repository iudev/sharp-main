<?php 

require_once 'includes/global.inc.php';
$page = "settings.php";

//Проверить вошел ли пользователь
if(!isset($_SESSION['logged_in'])) {
header("Location: login.php");
}


//взять объект user из сессии
$user = unserialize($_SESSION['user']);


//инициализировать php переменные, используемые в форме
$email = $user->email;
$username = $user->username;
$familiya = $user->familiya;
$imya = $user->imya;
$otchestvo = $user->otchestvo;
$data_rojd = $user->data_rojd;
$join_date = $user->join_date;
$school = $user->school;
$classs = $user->classs;
$coach = $user->coach;
$acc_type = $user->acc_type;
$division = $user->division;
$points = $user->points;
$error = "";
$message = "";


//проверить отправлена ли форма
if(isset($_POST['submit-settings'])) { 
//достать переменные $_POST 
$email = $_POST['email'];


$user->email = $email;
$user->save();


$message = "Settings Saved<br/>";
}
//Если форма не отправлена или не прошла проверку - показать форму снова

?>
<html>
	<head>
		<title>Мои ученики | Панель учителя | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		<main role="main">
			<center>
			<br><br>
			<h2>Панель учителя</h2>
			<h4>Ученики, указавшие меня:</h4>
			<?php
			$teacherfullname = $user->familiya." ".$user->imya." ".$user->otchestvo;
			 if ($result = $db->select_res('users','coach = "'.$teacherfullname.'"')) {
            echo '<table class="table">' .
            '<thead>' .
            '<tr>' .
            '<th>Никнейм</th>' .
            '<th>ФИО</th>' .
            '<th>Почта</th>' .
            '<th>Класс</th>' .
			'<th>Дивизион</th>' .
			'<th>Баллы</th>' .
            '</tr>' .
            '</thead>';
        while( $row = mysql_fetch_assoc($result)){
            echo '<tr>' .
                '<td>' . $row['username'] . '</td>' .
                '<td>' . $row['familiya'] . ' ' . $row['imya'] . ' ' . $row['otchestvo'] . '</td>' .
                '<td>' . $row['email'] . '</td>' .
                '<td>' . $row['classs'] . '</td>' .
				'<td>' . $row['division'] . '</td>' .
				'<td>' . $row['points'] . '</td>' .
                '</tr>';
        }
			 }
        echo '</table>';
			?>
			</center>
		</main>
	</body>
</html>