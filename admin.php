<?php
//index.php 
require_once 'includes/global.inc.php';
$page = "index.php";
$login = "";
$error = "";
$result1 = "";
$result2 = "";
$result3 = "";
if(isset($_POST['submit-search'])) { 

$login = $_POST['login'];

$userTools = new UserTools();
if($userTools->get_by_name($login)){
//удачный вход, редирект на страницу
$temp_user = $userTools->get_by_name($login);
if($temp_user->familiya == "") $error = "Пользователь не найден.";
$result1 = "ФИО: ".$temp_user->familiya." ".$temp_user->imya." ".$temp_user->otchestvo." ".$temp_user->data_rojd;
$result2 = "Класс: ".$temp_user->classs.". Школа: ".$temp_user->school.". Тренер: ".$temp_user->coach;
$result3 = "Дивизион: ".$temp_user->division.". Баллы: ".$temp_user->points;
}else{
$error = "Пользователь не найден.";
}
}
?>
<html>
	<head>
		<title>Админка | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		
		<main role="main">
		<?php $user = unserialize($_SESSION['user']); ?>
		
		<?php if(isset($_SESSION['logged_in']) && $user->admin > 0) : ?>
			<center>
			<h1>Админ-панель ШАРП</h1><br>
			<div class="row">
				<div class="col">
				  <h2>Поиск по имени</h2>
				  <form class="form-vertical" action="admin.php" method="post">
				 <fieldset>
				  <div class="form-group">
					  <label class="col control-label" for="login">Имя участника</label>  
					  <div class="col">
					  <input id="login" name="login" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $login; ?>"/> 
					  </div>
					</div>
					<div class="form-group">
					  <label class="col control-label" for="submit"></label>
					  <div class="col">
						<button value="submit-search" id="submit" name="submit-search" class="btn btn-success">Найти данные</button>
					  </div>
					</div>
					</fieldset>
					</form>
					<?php if($error != "") : ?>
					<div class="alert alert-danger" role="alert">
					  <strong>Ошибка</strong><br>
					  <?php echo $error; ?>
					 </div>
					<?php endif; ?>
					<?php if($error == "") : ?>
					<div class="alert alert-info" role="alert">
					  <strong>Получены сведения:</strong><br>
					  <?php echo $result1; ?><br>
					  <?php echo $result2; ?><br>
					  <?php echo $result3; ?>
					  </div>
					<?php endif; ?>
				</div>
				<div class="col">
				  <h2>Баллы</h2>
				  <a class="btn btn-success" href="add_points.php" role="button">Начисление</a><br><br>
				  <a class="btn btn-danger" href="rem_points.php" role="button">Снятие</a><br><br>
				  <a class="btn btn-info" href="div_change.php" role="button">Смена дивизиона</a><br><br>
				  <a class="btn btn-dark" href="div_change.php" role="button">Ручные уведомления</a>
				</div>
				<div class="col">
				  <h2>Модерирование вступающих</h2>
				  <div class="alert alert-warning" role="alert">
					  <strong>Модуль в разработке</strong>
					  <br>На данный момент модуль находится в разработке и недоступен для работы.
					</div>
				</div>
			  </div>
		</center>
		<?php else : ?>
			<div class="alert alert-danger" role="alert">
				  <strong>Ошибка безопасности #002</strong><br>
				  <p>Вы пытаетесь попасть на защищенную страницу.</p>
				  <hr>
				  <small>
 				 <p class="mb-0">There was admin.php GET request when parameter 'admin' is 0.<br>
				 Был совершен GET запрос на страницу администрирования (admin.php) без прав администратора.</p></small>
				</div>
		<?php endif; ?>
		</main>
		</body>
</html>