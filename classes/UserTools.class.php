<?php

//UserTools.class.php
require_once 'User.class.php';
require_once 'DB.class.php';

class UserTools {

//Log the user in. First checks to see if the
//username and password match a row in the database.
//If it is successful, set the session variables
//and store the user object within.
public function login($username, $password)

{

$hashedPassword = md5($password);
$result = mysql_query("SELECT * FROM users WHERE username = '$username' AND 
password = '$hashedPassword'");
if(mysql_num_rows($result) == 1)
{
$_SESSION["user"] = serialize(new User(mysql_fetch_assoc($result)));
$_SESSION["login_time"] = time();
$_SESSION["logged_in"] = 1;
return true;
}else{
return false;
}
}

//Log the user out. Destroy the session variables.
public function logout() {
unset($_SESSION['user']);
unset($_SESSION['login_time']);
unset($_SESSION['logged_in']);
session_destroy();
}
//Check to see if a username exists.
//This is called during registration to make sure all user names are unique.
public function checkUsernameExists($username) {
$result = mysql_query("select id from users where username='$username'");
if(mysql_num_rows($result) == 0)
{
return false;
}else{
return true;
}

}

//get a user
//returns a User object. Takes the users id as an input
public function get($id)
{
$db = new DB();
$result = $db->select('users', "id = $id");
return new User($result);
}

public function get_by_name($login)
{
$db = new DB();
$result = $db->select('users', "username = '$login'");
return new User($result);
}

public function add_points($login, $bally, $comment, $admin_user)
{
$db = new DB();
$result = $db->select('users', "username = '$login'");
$our_user = new User($result);
$temp_bally = (int)$our_user->points;
$write_bally = $temp_bally + $bally;
$our_user->points = (int)$write_bally;
$data = array(
"points" => "'$write_bally'"
);
$db->update($data, 'users', 'id = '.$our_user->id);
$data = array(
"username" => "'$login'",
"admin" => "'$admin_user'",
"date" => "'".date("Y-m-d H:i:s",time())."'",
"type" => "'Add'",
"before_balance" => "'$temp_bally'",
"change_value" => "'$bally'",
"after_balance" => "'$write_bally'",
"comment" => "'$comment'"
);
$db->insert($data, 'logs_points');
$text = "Администратор ".$admin_user." добавил ".$bally." баллов пользователю ".$login.". Было: ".$temp_bally.", стало ".$write_bally.". Комментарий администратора: ".$comment;
$data = array(
"username" => "'$login'",
"admin" => "'$admin_user'",
"date" => "'".date("Y-m-d H:i:s",time())."'",
"type" => "'Add Points'",
"text" => "'$text'"
);
$db->insert($data, 'log');
return $write_bally;
}

public function rem_points($login, $bally, $comment, $admin_user)
{
$db = new DB();
$result = $db->select('users', "username = '$login'");
$our_user = new User($result);
$temp_bally = (int)$our_user->points;
$write_bally = $temp_bally - $bally;
$our_user->points = (int)$write_bally;
$data = array(
"points" => "'$write_bally'"
);
$db->update($data, 'users', 'id = '.$our_user->id);
$data = array(
"username" => "'$login'",
"admin" => "'$admin_user'",
"date" => "'".date("Y-m-d H:i:s",time())."'",
"type" => "'Rem'",
"before_balance" => "'$temp_bally'",
"change_value" => "'$bally'",
"after_balance" => "'$write_bally'",
"comment" => "'$comment'"
);
$db->insert($data, 'logs_points');
$text = "Администратор ".$admin_user." снял ".$bally." баллов пользователю ".$login.". Было: ".$temp_bally.", стало ".$write_bally.". Комментарий администратора: ".$comment;
$data = array(
"username" => "'$login'",
"admin" => "'$admin_user'",
"date" => "'".date("Y-m-d H:i:s",time())."'",
"type" => "'Remove Points'",
"text" => "'$text'"
);
$db->insert($data, 'log');
return $write_bally;
}

public function custom($login, $comment, $admin_user)
{
$db = new DB();
$result = $db->select('users', "username = '$login'");
$our_user = new User($result);
$text = "Администратор ".$admin_user." отправил уведомление пользователю ".$login.". Текст: ".$comment;
$data = array(
"username" => "'$login'",
"admin" => "'$admin_user'",
"date" => "'".date("Y-m-d H:i:s",time())."'",
"type" => "'Notification'",
"text" => "'$text'"
);
$db->insert($data, 'log');
return true;
}

public function div_change($login, $bally, $comment, $admin_user)
{
$db = new DB();
$result = $db->select('users', "username = '$login'");
$our_user = new User($result);
$temp_bally = (int)$our_user->division;
$write_bally = $bally;
$our_user->division = (int)$write_bally;
$data = array(
"division" => "'$write_bally'"
);
$db->update($data, 'users', 'id = '.$our_user->id);
$data = array(
"username" => "'$login'",
"admin" => "'$admin_user'",
"date" => "'".date("Y-m-d H:i:s",time())."'",
"type" => "'Rem'",
"before_change" => "'$temp_bally'",
"after_change" => "'$write_bally'",
"comment" => "'$comment'"
);
$db->insert($data, 'logs_div');
$text = "Администратор ".$admin_user." сменил дивизион на ".$bally." пользователю ".$login.". Было: ".$temp_bally.", стало ".$write_bally.". Комментарий администратора: ".$comment;
$data = array(
"username" => "'$login'",
"admin" => "'$admin_user'",
"date" => "'".date("Y-m-d H:i:s",time())."'",
"type" => "'Division Change'",
"text" => "'$text'"
);
$db->insert($data, 'log');
return $write_bally;
}
}

?>