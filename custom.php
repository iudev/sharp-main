<?php
//index.php 
require_once '/home/admin/sites/obed.24.iu-dev.com/includes/global.inc.php';
$page = "custom.php";
$login = "";
$bally = "";
$comment = "";
$error = "";
$result1 = "";
$result2 = "";
$result3 = "";
$user1 = unserialize($_SESSION['user']);
$user2 = $user1->username;
if(isset($_POST['submit-custom'])) { 

$login = $_POST['login'];
$comment = $_POST['comment'];

$userTools = new UserTools();
if($userTools->get_by_name($login)){
//удачный вход, редирект на страницу
$temp_user = $userTools->get_by_name($login);
if($temp_user->familiya == "") $error = "Пользователь не найден.";
else{
$temp = $userTools->custom($login, $comment, $user2);
$result1 = "ФИО: ".$temp_user->familiya." ".$temp_user->imya." ".$temp_user->otchestvo." ".$temp_user->data_rojd;
$result2 = "Добавлено свободное уведомление.";
$result3 = "Текст: ".$comment;
}
}else{
$error = "Произошла ошибка.";
}
}
?>
<html>
	<head>
		<title>Свободное уведомление | Админка | ШАРП</title>
		<?php require_once 'includes/bootstrap.inc.php'; ?>
	</head>
	<body>
		<?php require_once 'includes/header.inc.php'; ?>
		
		<main role="main">
		<?php $user = unserialize($_SESSION['user']); ?>
		
		<?php if(isset($_SESSION['logged_in']) && $user->admin > 0) : ?>
			<center>
			<h1>Админ-панель ШАРП</h1><br>
			<div class="row">
				<div class="col">
				  <h2>Свободное уведомление</h2>
				  <form class="form-vertical" action="custom.php" method="post">
				 <fieldset>
				  <div class="form-group">
					  <label class="col control-label" for="login">Имя участника</label>  
					  <div class="col">
					  <input id="login" name="login" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $login; ?>"/> 
					  </div>
					</div>
					<div class="form-group">
					  <label class="col control-label" for="login">Уведомление</label>  
					  <div class="col">
					  <input id="comment" name="comment" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo $comment; ?>"/> 
					  </div>
					</div>
					<div class="form-group">
					  <label class="col control-label" for="submit"></label>
					  <div class="col">
						<button value="submit-custom" id="submit" name="submit-custom" class="btn btn-success">Выполнить запрос</button>
					  </div>
					</div>
					</fieldset>
					</form>
					<?php if($error != "") : ?>
					<div class="alert alert-danger" role="alert">
					  <strong>Ошибка</strong><br>
					  <?php echo $error; ?>
					 </div>
					<?php endif; ?>
					<?php if($error == "") : ?>
					<div class="alert alert-info" role="alert">
					  <strong>Получены сведения:</strong><br>
					  <?php echo $result1; ?><br>
					  <?php echo $result2; ?><br>
					  <?php echo $result3; ?>
					  </div>
					<?php endif; ?>
				</div>
			  </div>
		</center>
		<?php else : ?>
			<div class="alert alert-danger" role="alert">
				  <strong>Ошибка безопасности #002</strong><br>
				  <p>Вы пытаетесь попасть на защищенную страницу.</p>
				  <hr>
				  <small>
 				 <p class="mb-0">There was admin.php GET request when parameter 'admin' is 0.<br>
				 Был совершен GET запрос на страницу администрирования (admin.php) без прав администратора.</p></small>
				</div>
		<?php endif; ?>
		</main>
		</body>
</html>